.. role:: bash(code)
   :language: bash

=======
Procsea
=======

Procsea is an open-source voting and survey platform for cities or regions'
inhabitants. It empowers your will of democracy by giving them the power to
decide for the sake of their local government.

Built using Django_

Licensed under the Affero General Public License, for more informations see LICENSE_

.. _Django: https://www.djangoproject.com/
.. _LICENSE: LICENSE

Table of Contents
-----------------

* Quickstart_
* Roadmap_
* Contributing_
* FAQ_

.. _Quickstart: #quickstart
.. _Roadmap: #roadmap
.. _Contributing: #contributing
.. _FAQ: #faq

Quickstart
----------

To ease the burden of quickstarting the project, we recommend running the
project with Compose_

Running :bash:`docker-compose up` should get you a running environment with
the front-end being accessible on port 3500 and the backend available on port
3400.

.. _Compose: https://docs.docker.com/compose/

Roadmap
-------

- Complete the front-end.
- Makefile for migrations and stuff..
- Setup nginx as a proxy server on docker-compose
- Backend!

Contributing
------------

See CONTRIBUTING.rst_

.. _CONTRIBUTING.rst: CONTRIBUTING.rst 

FAQ
---

* Will there be mechanisms to fight against data corruption ?

Yes. The code will be designed so each changes done to the voting mechanism
will always restrict an admin to tamper with the data. It might be weak for
the first versions to come but any tips or papers is gladly welcomed.

* Why is the project licensed under the AGPL ?

If a user can access the platform then he must be able to access the source
code. As simple as that.

