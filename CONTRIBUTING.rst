============
Contributing
============

This notice serves as a basic set of mottos, indications and rules that should be
respected and understood before doing any contributions to it.

Violating these rules can result in a potential "ban", thus not allowing you to
contribute to this project at all.

Please note that a discussion channel is open on Matrix_

.. _Matrix : https://matrix.to/#/+procsea:matrix.org

Behavioral rules
----------------

- Do no harm.
- A criticism submitted on someone else's work have to be constructive.
- No arguments should be taken upon a personal view.

A post-scriptum: We as project maintainers, do our best to keep neutral upon our
work. We have our own views on life, on politics, on religion and whatever they
may be, we shall never let them influence our choices for the sake of this project.
We also swear not to be influenced by any personal characteristic from somebody
when deliberating around this person's work, whether it has been mentioned or shown
on the person's profile. The community around this tool should be a diverse and
mature community reuniting around the common goal of building a tool that stays true
to its original goal.

Issue reporting
---------------

- Please be clear, take the time to re-read your issue before posting it.
- Give as much context as possible for anybody to understand easily what you want to propose or report.

Pull Request
------------

- Please use the linting tools in place (also, the pre-commit tool).
- Document your code as much as possible.
- Ask for reviews.
